package com.cp4space.catagolue.servlets;

import java.lang.Math;
import java.io.PrintWriter;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import com.cp4space.catagolue.census.Tabulation;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.CommonNames;
import com.cp4space.catagolue.patterns.GolObject;
import com.cp4space.catagolue.utils.NumberUtils;
import com.cp4space.catagolue.utils.SvgUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class GolstatsServlet extends ExampleServlet {

    @Override
    public String getTitle(HttpServletRequest req) {

        return "Statistics";

    }

    @Override
    public void writeHead(PrintWriter writer, HttpServletRequest req) {

        writer.println("<script type=\"text/javascript\" src=\"/js/jquery.min.js\"></script>");

    }

    @Override
    public void writeContent(PrintWriter writer, HttpServletRequest req) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        String rulestring = "b3s23";
        String symmetry = "C1";

        Key statsKey = KeyFactory.createKey("Golstats", rulestring + "/" + symmetry);

        String rebootString = req.getParameter("reboot");
        boolean reboot = CensusServlet.isAdmin() && (rebootString != null);

        if (!reboot) {

        try {
            Entity statsEntity = datastore.get(statsKey);
            String content = ((Text) statsEntity.getProperty("data")).getValue();
            Date lastModified = (Date) statsEntity.getProperty("lastModified");

            Date currentTime = new Date();

            long elapsedMillis = currentTime.getTime() - lastModified.getTime();
            long elapsedSeconds = elapsedMillis / 1000;

            if (elapsedSeconds < 7200) {
                String creationTime = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(lastModified));
                writer.println("<p>The following page is cached from a copy generated at " + creationTime + ".");
                writer.println("It is " + elapsedSeconds + " seconds old.</p>");
                writer.println(content);
                return;
            }
        } catch (EntityNotFoundException e) {
            
        }

        }

        ByteArrayOutputStream bbuffer = new ByteArrayOutputStream();
        PrintWriter writer2 = new PrintWriter(bbuffer);
        generateContent(writer2, req, rulestring, symmetry, reboot);
        writer2.close();

        String content = bbuffer.toString();

        Entity statsEntity = new Entity("Golstats", rulestring + "/" + symmetry);
        Date date = new Date();
        statsEntity.setProperty("lastModified", date);
        statsEntity.setProperty("data", new Text(content));

        datastore.put(statsEntity);

        writer.println("<p>The following page has been generated from the current census data:</p>");
        writer.println(content);
    }

    public void generateContent(PrintWriter writer, HttpServletRequest req, String rulestring, String symmetry, boolean reboot) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        Map<String, String> namemap = new HashMap<String, String>();
        CommonNames.getNamemap("std", namemap, true);

        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        Query query = new Query("Tabulation", censusKey);
        PreparedQuery preparedQuery = datastore.prepare(query);
        List<Entity> tabulations = preparedQuery.asList(FetchOptions.Builder.withLimit(1000));

        Census census = new Census();

        int maxbits = 20;
        int largep = 5;

        for (Entity tabulation : tabulations) {

            String prefix = tabulation.getKey().getName();
            String preprefix = prefix.substring(0, 2);

            if (preprefix.equals("me")) { continue; }

            census.affineAdd(Tabulation.getCensusData(tabulation), 1);
            if (preprefix.equals("xs")) {
                maxbits = Math.max(maxbits,Integer.valueOf(prefix.substring(2)));
            }

        }


        TreeSet<Entry<String,Long>> sortedTable = census.sortedTable();

        Entity censusEntity;
        try {
            censusEntity = datastore.get(censusKey);

            long numsoups = (long) censusEntity.getProperty("numsoups");
            long numobjects = (long) censusEntity.getProperty("numobjects");

            writer.println("<p>From running " + NumberUtils.commaChameleon(numsoups) + " asymmetric 16-by-16 soups in B3/S23"
                    + ", a total of " + NumberUtils.commaChameleon(numobjects) + " objects of " + NumberUtils.commaChameleon(sortedTable.size())
                    + " distinct types were found.</p>");

            if (reboot) {
               long actual_numobjects = census.totobjs();
               long actual_numsoups = (long) (((double) actual_numobjects) / 21.845);
               writer.println("<p>Actual: " + actual_numobjects + ", " + actual_numsoups + "</p>");
               censusEntity.setProperty("numsoups", actual_numsoups);
               censusEntity.setProperty("numobjects", actual_numobjects);
               datastore.put(censusEntity);
            }

        } catch (EntityNotFoundException e) {
            // This will never happen.
        }

        long[] distinctOscillators = new long[largep];
        long[] distinctStills = new long[maxbits];
        long[] oeisStills = {0, 0, 0, 2, 1, 5, 4, 9, 10, 25, 46, 121, 240, 619, 1353, 3286,
            7773, 19044, 45759, 112243, 273188, 672172, 1646147, 4051732,
            9971377, 24619307, 60823008, 150613157};

        Map<String, Long> largeObjects = new HashMap<String, Long>();

        for (Entry<String, Long> entry : sortedTable) {

            String apgcode = entry.getKey();

            String prefix = apgcode.split("_")[0];

            int maxpop = 0;

            if (prefix.charAt(0) == 'x') {
                if (prefix.charAt(1) == 's') {
                    maxpop = Integer.valueOf(prefix.substring(2));
                    if ((maxpop >= 1) && (maxpop <= distinctStills.length)) {
                        distinctStills[maxpop - 1] += 1;
                    }
                } else {
                    if (prefix.charAt(1) == 'p') {
                        int period = Integer.valueOf(prefix.substring(2));
                        if ((period >= 0) && (period < largep)) {
                            distinctOscillators[period] += 1;
                        }
                    }
                    GolObject golObject = new GolObject("b3s23", apgcode);
                    maxpop = golObject.maxpop;
                }
            }

            if (maxpop >= 50) {

                largeObjects.put(apgcode, Long.valueOf(maxpop));

            }
        }

        writer.println("<h3>Distinct still-life counts</h3>");

        writer.println("<p>Here is the distribution of still-life counts, both in total and"
                + " in the census. A green bar indicates that all still-lifes of that population"
                + " have occurred naturally.</p");

        writer.println("<p>The upper line is from the Online Encyclopedia of Integer"
                + " Sequences (sequence <a href=\"http://oeis.org/A019473\">A019473</a>)."
                + " The bar chart was generated dynamically by Catagolue when"
                + " you visited this page; the same applies to the other charts and tables.</p>");

        writer.println("<script src=\"https://code.highcharts.com/highcharts.js\"></script>");
        writer.println("<script src=\"https://code.highcharts.com/highcharts-3d.js\"></script>");
        writer.println("<script src=\"https://code.highcharts.com/modules/exporting.js\"></script>");

        writer.println("<script type=\"text/javascript\">");
        writer.println("$(function () {");
        writer.println("    $('#distinctstills').highcharts({");
        writer.println("        chart: {backgroundColor: '#C0FFEE'},");
        writer.println("        title: {text: 'Distribution of distinct still-life counts'},");
        writer.println("        plotOptions: {series: {cursor: 'pointer', point: {events: {click: function () {location.href = this.options.url;}}}}},");
        writer.println("        xAxis: {tickInterval: 1},");
        writer.println("        yAxis: {type: 'logarithmic', minorTickInterval: 0.1, gridLineColor: '80BBAA', minorGridLineColor: '#A0DDCC'},");
        writer.println("        tooltip: {headerFormat: '<b>{series.name}</b><br />', pointFormat: '{point.y} still-lifes of size {point.x}'},");
        writer.println("        series: [{name: 'Naturally-occurring still-lifes', type: 'column', colorByPoint: true, pointStart: 1,");
        writer.println("            data: [{");
        for (int i = 0; i < distinctStills.length; i++) {
            if (i > 0) { writer.println("            }, {"); }
            writer.println("                y: "+String.valueOf(distinctStills[i])+",");
            writer.println("                url: '/census/b3s23/C1/xs"+String.valueOf(i + 1)+"'");
        }
        writer.println("            }],");
        String seriesData = "            colors: [";
        for (int i = 0; i < distinctStills.length; i++) {
            if (i > 0) {
                seriesData += ", ";
            }
            String colour = "'#a00000'";
            if (i < oeisStills.length) {
                if (distinctStills[i] >= oeisStills[i]) {
                    colour = "'#007f00'";
                } else {
                    long greenchannel_long = ((40000 * distinctStills[i]) / oeisStills[i]);
                    int greenchannel = (int) Math.sqrt(greenchannel_long);
                    String hexdigits = "0123456789abcdef";
                    colour = "'#a0" + hexdigits.charAt(greenchannel / 16) + hexdigits.charAt(greenchannel % 16) + "00'";
                }
            }
            seriesData += colour;
        }
        seriesData += "]";
        writer.println(seriesData);     
        writer.println("        }, {name: 'Total possible still-lifes', color: '#303030', pointStart: 1,");
        seriesData = "            data: [";
        for (int i = 0; i < oeisStills.length; i++) {
            if (i > 0) {
                seriesData += ", ";
            }
            seriesData += String.valueOf(oeisStills[i]);
        }
        seriesData += "]";
        writer.println(seriesData);
        writer.println("        }]");
        writer.println("    });");
        writer.println("});");
        writer.println("</script>");

        writer.println("<div id=\"distinctstills\" style=\"height: 560px\"></div>");

        writer.println("<h3>Naturally-occurring high-period oscillators</h3>");

        writer.println("<p>For the sake of brevity, the "
                + String.valueOf(distinctOscillators[2]) + " <a href=\"/census/b3s23/C1/xp2\">period-2</a>, "
                + String.valueOf(distinctOscillators[3]) + " <a href=\"/census/b3s23/C1/xp3\">period-3</a> and "
                + String.valueOf(distinctOscillators[4]) + " <a href=\"/census/b3s23/C1/xp4\">period-4</a> oscillators are tabulated elsewhere."
                + " All oscillators of period 5 or higher are given below:</p>");

        writer.println("<table cellspacing=1 border=2 cols=2>");
        writer.println("<tr><th>Object</th><th>Occurrences</th></tr>");

        for (Entry<String, Long> entry : sortedTable) {
            String apgcode = entry.getKey();
            if ((apgcode.charAt(0) == 'x') && (apgcode.charAt(1) == 'p')) {
                if (Long.valueOf(apgcode.split("_")[0].substring(2)) >= largep) {
                    if (namemap.containsKey(apgcode)) {
                        apgcode += " (" + namemap.get(apgcode) + ")";
                    }
                    SvgUtils.apgRow(false, -1, writer, rulestring, apgcode, String.valueOf(entry.getValue()));
                }
            }
        }

        writer.println("</table>");

        writer.println("<h3>Naturally-occurring non-stationary patterns</h3>");

        writer.println("<p>Most natural objects are still-lifes or oscillators. The following table includes "
                + "any spaceships or infinite-growth patterns that have appeared in the census.</p>");

        writer.println("<table cellspacing=1 border=2 cols=2>");
        writer.println("<tr><th>Object</th><th>Occurrences</th></tr>");

        for (Entry<String, Long> entry : sortedTable) {
            String apgcode = entry.getKey();
            if ((apgcode.charAt(0) != 'x') || (apgcode.charAt(1) == 'q')) {
                if (namemap.containsKey(apgcode)) {
                    apgcode += " (" + namemap.get(apgcode) + ")";
                }
                SvgUtils.apgRow(false, -1, writer, rulestring, apgcode, String.valueOf(entry.getValue()));
            }
        }

        writer.println("</table>");

        writer.println("<h3>Large objects</h3>");

        writer.println("<p>Very few natural objects have more than 50 live cells in any phase. "
                + "They are all tabulated below.</p>");

        writer.println("<table cellspacing=1 border=2 cols=2>");
        writer.println("<tr><th>Object</th><th>Maximum population</th></tr>");

        for (Entry<String, Long> entry : Census.sortTable(largeObjects)) {

            String apgcode = entry.getKey();
            if (namemap.containsKey(apgcode)) {
                apgcode += " (" + namemap.get(apgcode) + ")";
            }
            SvgUtils.apgRow(false, -1, writer, rulestring, apgcode, String.valueOf(entry.getValue()));
        }

        writer.println("</table>");

        writer.println("<h3>Common objects</h3>");

        writer.println("<p>The 100 most common objects are tabulated below:</p>");

        writer.println("<table cellspacing=1 border=2 cols=2>");
        writer.println("<tr><th>Rank</th><th>Image</th><th>Object</th><th>Occurrences</th></tr>");

        int i = 0;

        for (Entry<String, Long> entry : sortedTable) {
            i++;
            if ((i >= 1) && (i <= 100)) {
                String apgcode = entry.getKey();
                if (namemap.containsKey(apgcode)) {
                    apgcode += " (" + namemap.get(apgcode) + ")";
                }
                SvgUtils.apgRow(true, i, writer, rulestring, apgcode, String.valueOf(entry.getValue()));
            }
        }

        writer.println("</table>");
    }
}
